﻿' Copyright (C) 2013 Michael Senn

' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
' to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
' and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

' The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
' FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
' OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
' OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Imports System.IO

Public Class frmMain

    Private Const FormularNameFormatString As String = "{0} {1}"
    Private Const FormularBaseName As String = "Sorting strings"
    Private Const FormularWorkingName As String = "[Working...]"

    Private ClipboardIsFull As Boolean = True

    Friend CompareWords As Boolean = False
    Friend CompareUmlauts As Boolean = True

    Friend SelectedAlgorithm As Algorithm = Algorithm.Bubblesort

    Private Watch As New Stopwatch()

    Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        'Dim i() As Integer = {2, 5, 7, 4, 1, 6, 8, 9, 5, 3, 4, 7, 2}
        'Dim s As List(Of String) = StringToList("this is a triumphe, yadii, yaddaaaa.")

        'Dim i_sorted As List(Of String) = Sorting.Selectsort(s)
        'Dim t As String = ""
        'For Each item As String In i_sorted
        '    t &= item.ToString & "   "
        'Next

        'MsgBox(t)

        InitiateAlgorithmSelection()
        InitiateGUI()

        'Dim l As New List(Of String)
        'For i = 1 To 100
        '    l.Add(i)
        'Next

        'MsgBox(ListToString(l, ", "))
        'Sorting.Hush(l)
        'MsgBox(ListToString(l, ", "))

        'Application.Exit()

    End Sub


    Private Function StringToCharList(ByVal s As String) As List(Of String)

        Dim l As New List(Of String)

        For Each c As Char In s
            l.Add(c)
        Next

        Return l

    End Function

    Private Function StringToWordList(ByVal s As String, Optional ByVal Separator As String = " ") As List(Of String)

        ' String.Split takes a Char, and not a String, and returns an Array, and not a List(Of T). Therefore the following beauty.
        Return s.Split(Convert.ToChar(Separator)).ToList

    End Function

    Friend Function ListToString(ByVal l As List(Of String), Optional ByVal Separator As String = "") As String

        Return String.Join(Separator, l)
        'Dim SBuilder As New System.Text.StringBuilder()

        'For Each s As String In l
        '    SBuilder.Append(s)
        'Next

        'Return SBuilder.ToString()

    End Function

    Private Sub SetGUIBusy(ByVal Working As Boolean)

        If Working Then
            Me.Text = String.Format(FormularNameFormatString, FormularBaseName, FormularWorkingName)
            cmdSort.Enabled = False
            cmdReset.Enabled = False
            cmbAlgorithm.Enabled = False
            chkCompareWords.Enabled = False
            chkIgnoreUmlauts.Enabled = False
            toolMain.Enabled = False
            mnuMain.Enabled = False
        Else
            Me.Text = FormularBaseName
            cmdSort.Enabled = True
            cmdReset.Enabled = True
            cmbAlgorithm.Enabled = True
            chkCompareWords.Enabled = True
            chkIgnoreUmlauts.Enabled = True
            toolMain.Enabled = True
            mnuMain.Enabled = True
        End If

    End Sub

    Private Sub SetClipboardStatus(ByVal IsFull As Boolean)

        ClipboardIsFull = IsFull

        If ClipboardIsFull Then
            toolPaste.Enabled = True
            mnuiPaste.Enabled = True
        Else
            toolPaste.Enabled = False
            mnuiPaste.Enabled = False
        End If

    End Sub

    Private Sub InitiateAlgorithmSelection()

        cmbAlgorithm.Items.Add("Bubblesort")
        cmbAlgorithm.Items.Add("Ripplesort")
        cmbAlgorithm.Items.Add("Selectsort")
        cmbAlgorithm.Items.Add("Insertsort")
        cmbAlgorithm.Items.Add("Quicksort")
        cmbAlgorithm.Items.Add("Magicsort")
        cmbAlgorithm.Items.Add("Bogosort")

        If cmbAlgorithm.Items.Count > 0 Then
            cmbAlgorithm.SelectedIndex = 0
        End If

    End Sub

    Private Sub InitiateGUI()

        statuslblElapsedTime.Text = ""

        UpdateStatusLabel()
        UpdateTimeElapsedLabel()

        SetClipboardStatus(False)

        mnuichkIgnoreUmlauts.Checked = Not CompareUmlauts
        mnuichkCompareWords.Checked = CompareWords

        lblStatistics.Text = ""

        ' See http://www.codeproject.com/Questions/359467/Change-forecolor-of-text-when-textbox-is-readonly
        ' And no, it doesn't make terribly much sense.
        txtOutput.BackColor = Drawing.Color.FromKnownColor(KnownColor.Control)
        txtOutput.ReadOnly = True

    End Sub

    Private Sub UpdateSettingWords(ByVal CompareWordsNew As Boolean)

        CompareWords = CompareWordsNew

        mnuichkCompareWords.Checked = CompareWords
        chkCompareWords.Checked = CompareWords

    End Sub

    Private Sub UpdateSettingUmlauts(ByVal CompareUmlautsNew As Boolean)

        CompareUmlauts = CompareUmlautsNew

        mnuichkIgnoreUmlauts.Checked = Not CompareUmlauts
        chkIgnoreUmlauts.Checked = Not CompareUmlauts

    End Sub

    Private Sub UpdateSettingAlgorithm(ByVal NewAlgorithm As Algorithm)
        SelectedAlgorithm = NewAlgorithm

        Select Case SelectedAlgorithm
            Case Algorithm.UNKNOWN
                cmbAlgorithm.SelectedIndex = 0
                SelectedAlgorithm = Algorithm.Bubblesort
            Case Else
                cmbAlgorithm.SelectedIndex = SelectedAlgorithm
        End Select

    End Sub

    Private Sub UpdateStatusLabel()

        Dim StatusUmlauts As String
        Dim StatusType As String

        If CompareUmlauts Then
            StatusUmlauts = "Comparing umlauts"
        Else
            StatusUmlauts = "Ignoring umlauts"
        End If

        If CompareWords Then
            StatusType = "Comparing words"
        Else
            StatusType = "Comparing characters"
        End If

        statuslblStatus.Text = String.Format("{0}; {1}", StatusUmlauts, StatusType)

    End Sub

    Private Sub UpdateStatistics()

        Dim count_letters As Integer = txtInput.Text.Length
        Dim count_words As Integer = txtInput.Text.Split(Convert.ToChar(" ")).Length

        Dim arg() As String = {vbCrLf}
        Dim count_lines As Integer = txtInput.Text.Split(arg, StringSplitOptions.None).Length

        lblStatistics.Text = String.Format("Letters: {1}{0}{0}Words: {2}{0}{0}Lines: {3}{0}{0}", vbCrLf, count_letters, count_words, count_lines)

    End Sub

    Private Sub UpdateTimeElapsedLabel()
        statuslblElapsedTime.Text = String.Format("Elapsed time: {0} seconds.", Watch.ElapsedMilliseconds / 1000)
    End Sub

    Private Sub ReadFromFile()

        Dim ofd As New OpenFileDialog()
        ofd.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*"

        If ofd.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            Return
        End If

        Dim path As String = ofd.FileName

        Dim sb As New System.Text.StringBuilder()

        Try

            Dim sr As New StreamReader(path)

            Do Until sr.Peek() = -1
                sb.Append(sr.ReadLine())
                sb.Append(vbCrLf)
            Loop
            sr.Close()

        Catch ex As IOException

            MsgBox(ex.Message)

        End Try

        ClearGUI()
        txtInput.Text = sb.ToString()

    End Sub

    Private Sub WriteToFile(ByVal Text As String)

        Dim sfd As New SaveFileDialog()
        sfd.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*"

        If sfd.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            Return
        End If

        Dim path As String = sfd.FileName

        Try

            Dim sr As New StreamWriter(path)
            sr.Write(Text)
            sr.Close()

        Catch ex As IOException

            MsgBox(ex.Message)

        End Try


    End Sub

    Private Sub WriteToClipboard(Optional ByVal Cut As Boolean = False)

        If TypeOf ActiveControl Is TextBox Then
            Dim ActiveTextbox As TextBox = DirectCast(ActiveControl, TextBox)

            If ActiveTextbox.Text <> String.Empty Then
                If Cut Then
                    ActiveTextbox.Cut()
                Else
                    ActiveTextbox.Copy()
                End If
                SetClipboardStatus(True)
            End If

            'MsgBox(Clipboard.GetText())

        End If

    End Sub

    Private Sub ReadFromClipboard()

        If ClipboardIsFull Then
            If TypeOf ActiveControl Is TextBox Then
                Dim ActiveTextbox As TextBox = DirectCast(ActiveControl, TextBox)
                ActiveTextbox.Paste()
            End If
        End If

    End Sub

    Private Sub ClearGUI()

        txtInput.Text = ""
        txtOutput.Text = ""

        ClearTimeElapsed()

    End Sub

    Private Sub ClearTimeElapsed()

        Watch.Reset()
        UpdateTimeElapsedLabel()

    End Sub

    Private Sub Sort()

        Sorting.IMALIVE = True

        Dim l As List(Of String)
        Dim l_sorted As List(Of String)

        If CompareWords Then
            l = StringToWordList(txtInput.Text)
        Else
            l = StringToCharList(txtInput.Text)
        End If


        SetGUIBusy(True)

        Watch.Start()
        Select Case SelectedAlgorithm
            Case Algorithm.Bubblesort
                l_sorted = Sorting.Bubblesort(l)
            Case Algorithm.Ripplesort
                l_sorted = Sorting.Ripplesort(l)
            Case Algorithm.Selectsort
                l_sorted = Sorting.Selectsort(l)
            Case Algorithm.Insertsort
                l_sorted = Sorting.Insertsort(l)
            Case Algorithm.Quicksort
                l_sorted = Sorting.Quicksort(l)
            Case Algorithm.Magicsort
                l_sorted = Sorting.Magicsort(l)
            Case Algorithm.Bogosort
                l_sorted = Sorting.Bogosort(l)
            Case Algorithm.UNKNOWN
                l_sorted = New List(Of String)
                MsgBox("Uh, I don't know what to do D:.")
        End Select
        Watch.Stop()

        SetGUIBusy(False)
        UpdateTimeElapsedLabel()

        If CompareWords Then
            txtOutput.Text = ListToString(l_sorted, " ")
        Else
            txtOutput.Text = ListToString(l_sorted)
        End If
        Watch.Reset()

    End Sub


    Private Sub cmdSort_Click(sender As System.Object, e As System.EventArgs) Handles cmdSort.Click
        Sort()
    End Sub

    Private Sub toolSort_Click(sender As System.Object, e As System.EventArgs) Handles toolSort.Click
        Sort()
    End Sub

    Private Sub mnuiSort_Click(sender As System.Object, e As System.EventArgs) Handles mnuiSort.Click
        Sort()
    End Sub

    Private Sub cmdReset_Click(sender As System.Object, e As System.EventArgs) Handles cmdReset.Click
        ClearGUI()
    End Sub

    Private Sub toolNew_Click(sender As System.Object, e As System.EventArgs) Handles toolNew.Click
        ClearGUI()
    End Sub

    Private Sub mnuiNew_Click(sender As System.Object, e As System.EventArgs) Handles mnuiNew.Click
        ClearGUI()
    End Sub


    Private Sub cmdKill_Click(sender As System.Object, e As System.EventArgs) Handles cmdKill.Click

        Sorting.IMALIVE = False

    End Sub


    Private Sub mnuiGenerateASCII_Click(sender As System.Object, e As System.EventArgs) Handles mnuiGenerateASCII.Click

        Dim StringLength As String = InputBox("How long should the string be?", "Generating random ASCII string", "10000")
        If StringLength = String.Empty Then
            Return
        Else
            Me.txtInput.Text = TextGenerator.ASCII(Convert.ToInt32(StringLength))
        End If

    End Sub

    Private Sub mnuiGenerateLoremIpsum_Click(sender As System.Object, e As System.EventArgs) Handles mnuiGenerateLoremIpsum.Click

        Dim StringLength As String = InputBox("How long should the string be?", "Generating random ASCII string", "10000")
        If StringLength = String.Empty Then
            Return
        Else
            Me.txtInput.Text = TextGenerator.LoremIpsum(Convert.ToInt32(StringLength))
        End If

    End Sub

    Private Sub toolOpen_Click(sender As System.Object, e As System.EventArgs) Handles toolOpen.Click
        ReadFromFile()
    End Sub

    Private Sub mnuiOpen_Click(sender As System.Object, e As System.EventArgs) Handles mnuiOpen.Click
        ReadFromFile()
    End Sub

    Private Sub mnuiSaveBoth_Click(sender As System.Object, e As System.EventArgs) Handles mnuiSaveBoth.Click
        WriteToFile(txtInput.Text & vbCrLf & txtOutput.Text)
    End Sub

    Private Sub mnuiSaveOutput_Click(sender As System.Object, e As System.EventArgs) Handles mnuiSaveOutput.Click
        WriteToFile(txtOutput.Text)
    End Sub

    Private Sub toolSave_Click(sender As System.Object, e As System.EventArgs) Handles toolSave.Click
        WriteToFile(txtOutput.Text)
    End Sub

    Private Sub mnuiSaveInput_Click(sender As System.Object, e As System.EventArgs) Handles mnuiSaveInput.Click
        WriteToFile(txtInput.Text)
    End Sub


    Private Sub mnuiQuit_Click(sender As System.Object, e As System.EventArgs) Handles mnuiQuit.Click
        Application.Exit()
    End Sub



    Private Sub mnuichkIgnoreUmlauts_Click(sender As System.Object, e As System.EventArgs) Handles mnuichkIgnoreUmlauts.Click

        UpdateSettingUmlauts(Not mnuichkIgnoreUmlauts.Checked)
        UpdateStatusLabel()

    End Sub

    Private Sub chkIgnoreUmlauts_Click(sender As System.Object, e As System.EventArgs) Handles chkIgnoreUmlauts.Click

        UpdateSettingUmlauts(Not chkIgnoreUmlauts.Checked)
        UpdateStatusLabel()

    End Sub

    Private Sub mnuichkCompareWords_Click(sender As System.Object, e As System.EventArgs) Handles mnuichkCompareWords.Click

        UpdateSettingWords(mnuichkCompareWords.Checked)
        UpdateStatusLabel()

    End Sub

    Private Sub chkCompareWords_Click(sender As System.Object, e As System.EventArgs) Handles chkCompareWords.Click

        UpdateSettingWords(chkCompareWords.Checked)
        UpdateStatusLabel()

    End Sub




    Private Sub mnuiFormColour_Click(sender As System.Object, e As System.EventArgs) Handles mnuiFormColour.Click

        Dim Dlg As New ColorDialog()

        Dlg.Color = Me.BackColor

        If Dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.BackColor = Dlg.Color
        End If

    End Sub

    Private Sub mnuiTextboxColour_Click(sender As System.Object, e As System.EventArgs) Handles mnuiTextboxColour.Click

        Dim Dlg As New ColorDialog()

        Dlg.Color = Me.BackColor

        If Dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtInput.ForeColor = Dlg.Color
            txtOutput.ForeColor = Dlg.Color
        End If

    End Sub

    Private Sub txtInput_TextChanged(sender As Object, e As System.EventArgs) Handles txtInput.TextChanged
        UpdateStatistics()
    End Sub

    Private Sub toolPaste_Click(sender As System.Object, e As System.EventArgs) Handles toolPaste.Click
        ReadFromClipboard()
    End Sub

    Private Sub mnuiPaste_Click(sender As System.Object, e As System.EventArgs) Handles mnuiPaste.Click
        ReadFromClipboard()
    End Sub

    Private Sub toolCut_Click(sender As System.Object, e As System.EventArgs) Handles toolCut.Click
        WriteToClipboard(True)
    End Sub

    Private Sub mnuiCut_Click(sender As System.Object, e As System.EventArgs) Handles mnuiCut.Click
        WriteToClipboard(True)
    End Sub

    Private Sub toolCopy_Click(sender As System.Object, e As System.EventArgs) Handles toolCopy.Click
        WriteToClipboard()
    End Sub

    Private Sub mnuiCopy_Click(sender As System.Object, e As System.EventArgs) Handles mnuiCopy.Click
        WriteToClipboard()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        frmAbout.Show()
    End Sub


    Private Sub cmbAlgorithm_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbAlgorithm.SelectedIndexChanged

        Select Case cmbAlgorithm.SelectedIndex
            Case 0
                SelectedAlgorithm = Algorithm.Bubblesort
            Case 1
                SelectedAlgorithm = Algorithm.Ripplesort
            Case 2
                SelectedAlgorithm = Algorithm.Selectsort
            Case 3
                SelectedAlgorithm = Algorithm.Insertsort
            Case 4
                SelectedAlgorithm = Algorithm.Quicksort
            Case 5
                SelectedAlgorithm = Algorithm.Magicsort
            Case 6
                SelectedAlgorithm = Algorithm.Bogosort
            Case Else
                SelectedAlgorithm = Algorithm.UNKNOWN
        End Select

    End Sub

End Class
