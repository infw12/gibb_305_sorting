﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Allgemeine Informationen über eine Assembly werden über die folgenden 
' Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
' die mit einer Assembly verknüpft sind.

' Die Werte der Assemblyattribute überprüfen

<Assembly: AssemblyTitle("Sortieralgorithmen")> 
<Assembly: AssemblyDescription("This application sorts strings. Icons by Jan Kovařík (http://glyphicons.com/). Licensed under the MIT license.")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("Gibb - 318 - Sortieralgorithmen")> 
<Assembly: AssemblyCopyright("Copyright ©  2012 Michael Senn")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
<Assembly: Guid("ac59db43-6c64-43e5-a79f-ae113ab64abf")> 

' Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
'
'      Hauptversion
'      Nebenversion 
'      Buildnummer
'      Revision
'
' Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
' übernehmen, indem Sie "*" eingeben:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
