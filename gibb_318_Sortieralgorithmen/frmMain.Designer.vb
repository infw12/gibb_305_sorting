﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtInput = New System.Windows.Forms.TextBox()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.cmdSort = New System.Windows.Forms.Button()
        Me.cmbAlgorithm = New System.Windows.Forms.ComboBox()
        Me.cmdReset = New System.Windows.Forms.Button()
        Me.cmdKill = New System.Windows.Forms.Button()
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiSaveInput = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiSaveBoth = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiSaveOutput = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuiQuit = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiSort = New System.Windows.Forms.ToolStripMenuItem()
        Me.PreferencesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColourToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiFormColour = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiTextboxColour = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuichkIgnoreUmlauts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuichkCompareWords = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiGenerateLoremIpsum = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuiGenerateASCII = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.statuslblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.statuslblElapsedTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toolMain = New System.Windows.Forms.ToolStrip()
        Me.toolNew = New System.Windows.Forms.ToolStripButton()
        Me.toolOpen = New System.Windows.Forms.ToolStripButton()
        Me.toolSave = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.toolCopy = New System.Windows.Forms.ToolStripButton()
        Me.toolCut = New System.Windows.Forms.ToolStripButton()
        Me.toolPaste = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.toolSort = New System.Windows.Forms.ToolStripButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkIgnoreUmlauts = New System.Windows.Forms.CheckBox()
        Me.chkCompareWords = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblStatistics = New System.Windows.Forms.Label()
        Me.mnuMain.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.toolMain.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtInput
        '
        Me.txtInput.Location = New System.Drawing.Point(10, 50)
        Me.txtInput.MaxLength = 500000
        Me.txtInput.Multiline = True
        Me.txtInput.Name = "txtInput"
        Me.txtInput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInput.Size = New System.Drawing.Size(640, 280)
        Me.txtInput.TabIndex = 0
        '
        'txtOutput
        '
        Me.txtOutput.Location = New System.Drawing.Point(10, 350)
        Me.txtOutput.MaxLength = 600000
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOutput.Size = New System.Drawing.Size(640, 290)
        Me.txtOutput.TabIndex = 1
        '
        'cmdSort
        '
        Me.cmdSort.Location = New System.Drawing.Point(660, 160)
        Me.cmdSort.Name = "cmdSort"
        Me.cmdSort.Size = New System.Drawing.Size(180, 50)
        Me.cmdSort.TabIndex = 2
        Me.cmdSort.Text = "Sort"
        Me.cmdSort.UseVisualStyleBackColor = True
        '
        'cmbAlgorithm
        '
        Me.cmbAlgorithm.FormattingEnabled = True
        Me.cmbAlgorithm.Location = New System.Drawing.Point(80, 20)
        Me.cmbAlgorithm.Name = "cmbAlgorithm"
        Me.cmbAlgorithm.Size = New System.Drawing.Size(90, 21)
        Me.cmbAlgorithm.TabIndex = 3
        '
        'cmdReset
        '
        Me.cmdReset.Location = New System.Drawing.Point(660, 210)
        Me.cmdReset.Name = "cmdReset"
        Me.cmdReset.Size = New System.Drawing.Size(90, 40)
        Me.cmdReset.TabIndex = 2
        Me.cmdReset.Text = "Reset"
        Me.cmdReset.UseVisualStyleBackColor = True
        '
        'cmdKill
        '
        Me.cmdKill.Location = New System.Drawing.Point(750, 210)
        Me.cmdKill.Name = "cmdKill"
        Me.cmdKill.Size = New System.Drawing.Size(90, 40)
        Me.cmdKill.TabIndex = 2
        Me.cmdKill.Text = "Kill"
        Me.cmdKill.UseVisualStyleBackColor = True
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.mnuiSort, Me.PreferencesToolStripMenuItem, Me.GenerateToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(853, 24)
        Me.mnuMain.TabIndex = 4
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuiNew, Me.mnuiOpen, Me.mnuiSave, Me.ToolStripSeparator1, Me.mnuiQuit})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'mnuiNew
        '
        Me.mnuiNew.Name = "mnuiNew"
        Me.mnuiNew.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuiNew.Size = New System.Drawing.Size(148, 22)
        Me.mnuiNew.Text = "New"
        '
        'mnuiOpen
        '
        Me.mnuiOpen.Name = "mnuiOpen"
        Me.mnuiOpen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.mnuiOpen.Size = New System.Drawing.Size(148, 22)
        Me.mnuiOpen.Text = "Open"
        '
        'mnuiSave
        '
        Me.mnuiSave.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuiSaveInput, Me.mnuiSaveBoth, Me.mnuiSaveOutput})
        Me.mnuiSave.Name = "mnuiSave"
        Me.mnuiSave.Size = New System.Drawing.Size(148, 22)
        Me.mnuiSave.Text = "Save"
        '
        'mnuiSaveInput
        '
        Me.mnuiSaveInput.Name = "mnuiSaveInput"
        Me.mnuiSaveInput.Size = New System.Drawing.Size(181, 22)
        Me.mnuiSaveInput.Text = "Save input"
        '
        'mnuiSaveBoth
        '
        Me.mnuiSaveBoth.Name = "mnuiSaveBoth"
        Me.mnuiSaveBoth.Size = New System.Drawing.Size(181, 22)
        Me.mnuiSaveBoth.Text = "Save input && output"
        '
        'mnuiSaveOutput
        '
        Me.mnuiSaveOutput.Name = "mnuiSaveOutput"
        Me.mnuiSaveOutput.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuiSaveOutput.Size = New System.Drawing.Size(181, 22)
        Me.mnuiSaveOutput.Text = "Save output"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(145, 6)
        '
        'mnuiQuit
        '
        Me.mnuiQuit.Name = "mnuiQuit"
        Me.mnuiQuit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.mnuiQuit.Size = New System.Drawing.Size(148, 22)
        Me.mnuiQuit.Text = "Quit"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuiCopy, Me.mnuiCut, Me.mnuiPaste})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'mnuiCopy
        '
        Me.mnuiCopy.Name = "mnuiCopy"
        Me.mnuiCopy.Size = New System.Drawing.Size(152, 22)
        Me.mnuiCopy.Text = "Copy"
        '
        'mnuiCut
        '
        Me.mnuiCut.Name = "mnuiCut"
        Me.mnuiCut.Size = New System.Drawing.Size(152, 22)
        Me.mnuiCut.Text = "Cut"
        '
        'mnuiPaste
        '
        Me.mnuiPaste.Name = "mnuiPaste"
        Me.mnuiPaste.Size = New System.Drawing.Size(152, 22)
        Me.mnuiPaste.Text = "Paste"
        '
        'mnuiSort
        '
        Me.mnuiSort.Name = "mnuiSort"
        Me.mnuiSort.Size = New System.Drawing.Size(40, 20)
        Me.mnuiSort.Text = "Sort"
        '
        'PreferencesToolStripMenuItem
        '
        Me.PreferencesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ColourToolStripMenuItem, Me.mnuichkIgnoreUmlauts, Me.mnuichkCompareWords})
        Me.PreferencesToolStripMenuItem.Name = "PreferencesToolStripMenuItem"
        Me.PreferencesToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.PreferencesToolStripMenuItem.Text = "Preferences"
        '
        'ColourToolStripMenuItem
        '
        Me.ColourToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuiFormColour, Me.mnuiTextboxColour})
        Me.ColourToolStripMenuItem.Name = "ColourToolStripMenuItem"
        Me.ColourToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.ColourToolStripMenuItem.Text = "Colour"
        '
        'mnuiFormColour
        '
        Me.mnuiFormColour.Name = "mnuiFormColour"
        Me.mnuiFormColour.Size = New System.Drawing.Size(102, 22)
        Me.mnuiFormColour.Text = "Form"
        '
        'mnuiTextboxColour
        '
        Me.mnuiTextboxColour.Name = "mnuiTextboxColour"
        Me.mnuiTextboxColour.Size = New System.Drawing.Size(102, 22)
        Me.mnuiTextboxColour.Text = "Text"
        '
        'mnuichkIgnoreUmlauts
        '
        Me.mnuichkIgnoreUmlauts.CheckOnClick = True
        Me.mnuichkIgnoreUmlauts.Name = "mnuichkIgnoreUmlauts"
        Me.mnuichkIgnoreUmlauts.Size = New System.Drawing.Size(158, 22)
        Me.mnuichkIgnoreUmlauts.Text = "Ignore Umlauts"
        '
        'mnuichkCompareWords
        '
        Me.mnuichkCompareWords.CheckOnClick = True
        Me.mnuichkCompareWords.Name = "mnuichkCompareWords"
        Me.mnuichkCompareWords.Size = New System.Drawing.Size(158, 22)
        Me.mnuichkCompareWords.Text = "Compare words"
        '
        'GenerateToolStripMenuItem
        '
        Me.GenerateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuiGenerateLoremIpsum, Me.mnuiGenerateASCII})
        Me.GenerateToolStripMenuItem.Name = "GenerateToolStripMenuItem"
        Me.GenerateToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.GenerateToolStripMenuItem.Text = "Generate"
        '
        'mnuiGenerateLoremIpsum
        '
        Me.mnuiGenerateLoremIpsum.Name = "mnuiGenerateLoremIpsum"
        Me.mnuiGenerateLoremIpsum.Size = New System.Drawing.Size(150, 22)
        Me.mnuiGenerateLoremIpsum.Text = "Lorem Ipsum"
        '
        'mnuiGenerateASCII
        '
        Me.mnuiGenerateASCII.Name = "mnuiGenerateASCII"
        Me.mnuiGenerateASCII.Size = New System.Drawing.Size(150, 22)
        Me.mnuiGenerateASCII.Text = "Random ASCII"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statuslblStatus, Me.ToolStripStatusLabel2, Me.statuslblElapsedTime})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 645)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(853, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'statuslblStatus
        '
        Me.statuslblStatus.Name = "statuslblStatus"
        Me.statuslblStatus.Size = New System.Drawing.Size(39, 17)
        Me.statuslblStatus.Text = "Status"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel2.Text = "|"
        Me.ToolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'statuslblElapsedTime
        '
        Me.statuslblElapsedTime.Name = "statuslblElapsedTime"
        Me.statuslblElapsedTime.Size = New System.Drawing.Size(74, 17)
        Me.statuslblElapsedTime.Text = "ElapsedTime"
        '
        'toolMain
        '
        Me.toolMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolNew, Me.toolOpen, Me.toolSave, Me.ToolStripSeparator3, Me.toolCopy, Me.toolCut, Me.toolPaste, Me.ToolStripSeparator4, Me.toolSort})
        Me.toolMain.Location = New System.Drawing.Point(0, 24)
        Me.toolMain.Name = "toolMain"
        Me.toolMain.Size = New System.Drawing.Size(853, 25)
        Me.toolMain.TabIndex = 6
        Me.toolMain.Text = "ToolStrip1"
        '
        'toolNew
        '
        Me.toolNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolNew.Image = Global.gibb_318_Sortieralgorithmen.My.Resources.Resources.imgNew
        Me.toolNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolNew.Name = "toolNew"
        Me.toolNew.Size = New System.Drawing.Size(23, 22)
        Me.toolNew.Text = "New"
        '
        'toolOpen
        '
        Me.toolOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolOpen.Image = Global.gibb_318_Sortieralgorithmen.My.Resources.Resources.imgOpen
        Me.toolOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolOpen.Name = "toolOpen"
        Me.toolOpen.Size = New System.Drawing.Size(23, 22)
        Me.toolOpen.Text = "Open"
        '
        'toolSave
        '
        Me.toolSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolSave.Image = Global.gibb_318_Sortieralgorithmen.My.Resources.Resources.imgSave
        Me.toolSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolSave.Name = "toolSave"
        Me.toolSave.Size = New System.Drawing.Size(23, 22)
        Me.toolSave.Text = "Save"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'toolCopy
        '
        Me.toolCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolCopy.Image = Global.gibb_318_Sortieralgorithmen.My.Resources.Resources.imgCopy
        Me.toolCopy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolCopy.Name = "toolCopy"
        Me.toolCopy.Size = New System.Drawing.Size(23, 22)
        Me.toolCopy.Text = "Copy"
        '
        'toolCut
        '
        Me.toolCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolCut.Image = Global.gibb_318_Sortieralgorithmen.My.Resources.Resources.imgCut
        Me.toolCut.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolCut.Name = "toolCut"
        Me.toolCut.Size = New System.Drawing.Size(23, 22)
        Me.toolCut.Text = "Cut"
        '
        'toolPaste
        '
        Me.toolPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolPaste.Image = Global.gibb_318_Sortieralgorithmen.My.Resources.Resources.imgPaste
        Me.toolPaste.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolPaste.Name = "toolPaste"
        Me.toolPaste.Size = New System.Drawing.Size(23, 22)
        Me.toolPaste.Text = "Paste"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'toolSort
        '
        Me.toolSort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolSort.Image = Global.gibb_318_Sortieralgorithmen.My.Resources.Resources.imgSort
        Me.toolSort.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolSort.Name = "toolSort"
        Me.toolSort.Size = New System.Drawing.Size(23, 22)
        Me.toolSort.Text = "Sort"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Location = New System.Drawing.Point(10, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 20)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Algorithm"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkIgnoreUmlauts)
        Me.GroupBox1.Controls.Add(Me.chkCompareWords)
        Me.GroupBox1.Controls.Add(Me.cmbAlgorithm)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(660, 50)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(180, 100)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sorting"
        '
        'chkIgnoreUmlauts
        '
        Me.chkIgnoreUmlauts.Location = New System.Drawing.Point(10, 50)
        Me.chkIgnoreUmlauts.Name = "chkIgnoreUmlauts"
        Me.chkIgnoreUmlauts.Size = New System.Drawing.Size(160, 17)
        Me.chkIgnoreUmlauts.TabIndex = 8
        Me.chkIgnoreUmlauts.Text = "Ignore Umlauts"
        Me.chkIgnoreUmlauts.UseVisualStyleBackColor = True
        '
        'chkCompareWords
        '
        Me.chkCompareWords.Location = New System.Drawing.Point(10, 70)
        Me.chkCompareWords.Name = "chkCompareWords"
        Me.chkCompareWords.Size = New System.Drawing.Size(160, 24)
        Me.chkCompareWords.TabIndex = 8
        Me.chkCompareWords.Text = "Compare Words"
        Me.chkCompareWords.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblStatistics)
        Me.GroupBox2.Location = New System.Drawing.Point(660, 350)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(180, 290)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Statistics"
        '
        'lblStatistics
        '
        Me.lblStatistics.Location = New System.Drawing.Point(10, 20)
        Me.lblStatistics.Name = "lblStatistics"
        Me.lblStatistics.Size = New System.Drawing.Size(160, 260)
        Me.lblStatistics.TabIndex = 0
        Me.lblStatistics.Text = "Statistics"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(853, 667)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.toolMain)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.cmdKill)
        Me.Controls.Add(Me.cmdReset)
        Me.Controls.Add(Me.cmdSort)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.txtInput)
        Me.Controls.Add(Me.mnuMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MainMenuStrip = Me.mnuMain
        Me.Name = "frmMain"
        Me.Text = "Sorting Strings"
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.toolMain.ResumeLayout(False)
        Me.toolMain.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtInput As System.Windows.Forms.TextBox
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents cmdSort As System.Windows.Forms.Button
    Friend WithEvents cmbAlgorithm As System.Windows.Forms.ComboBox
    Friend WithEvents cmdReset As System.Windows.Forms.Button
    Friend WithEvents cmdKill As System.Windows.Forms.Button
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiQuit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiCut As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiPaste As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PreferencesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuichkIgnoreUmlauts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColourToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiFormColour As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiTextboxColour As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GenerateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiGenerateLoremIpsum As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiGenerateASCII As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents statuslblElapsedTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents mnuichkCompareWords As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents statuslblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents mnuiSaveInput As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiSaveOutput As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuiSaveBoth As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents toolMain As System.Windows.Forms.ToolStrip
    Friend WithEvents toolNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents toolCopy As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolCut As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolPaste As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents toolSort As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkIgnoreUmlauts As System.Windows.Forms.CheckBox
    Friend WithEvents chkCompareWords As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatistics As System.Windows.Forms.Label
    Friend WithEvents mnuiSort As System.Windows.Forms.ToolStripMenuItem

End Class
