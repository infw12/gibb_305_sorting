﻿' Copyright (C) 2013 Michael Senn

' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
' to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
' and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

' The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
' FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
' OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
' OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Module TextGenerator

    Friend Function LoremIpsum(Optional ByVal Length As Integer = 500) As String

        Dim ExampleString As String = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."

        Dim SB As New System.Text.StringBuilder()
        ' If we want a string of length 500, and our example string is 50 characters long, we'll need ten times the example string. That's the multiplier.
        Dim Multiplier As Double = Length / ExampleString.Length

        ' Assuming desired length = 120, example string's length = 50, therefore multiplier = 2.4.
        ' First we'll add the example strings to the stringbuilder twice, leaving us with a length of 2 * 50 = 100.
        For i = 1 To Math.Floor(Multiplier)
            SB.Append(ExampleString)
        Next

        ' Now we'll get the difference between the desired (120) and the current (100) length. In this example's case that'd be 20.
        ' With that done, we'll just add another 20 characters, taken from the example string, to the stringbuilder.
        Dim RemainingLength As Integer = Length - SB.Length
        SB.Append(ExampleString.Substring(0, RemainingLength))

        Return SB.ToString()

    End Function

    Friend Function ASCII(Optional ByVal Length As Integer = 500, Optional ByVal AverageWordLength As Integer = 7) As String
        Dim r As New Random()
        Dim SB As New System.Text.StringBuilder()

        ' Rather self explanatory. We'll generate a random ASCII character as many times as the desired length is.
        For i = 0 To Length - 1
            SB.Append(Chr(r.Next(32, 126)))

            ' Todo: Uhm, it's not really correct, but it works well enough.
            If Math.Round(r.Next(1, AverageWordLength) + r.Next(1, AverageWordLength)) = AverageWordLength Then
                SB.Append(" ")
                ' Spaces count as signs too, so we'll have to create one character less.
                i += 1
            End If
        Next


        Return SB.ToString()

    End Function

End Module
