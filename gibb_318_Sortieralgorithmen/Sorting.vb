﻿' Copyright (C) 2013 Michael Senn

' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
' to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
' and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

' The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
' FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
' OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
' OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

' Can be set to 'binary' to compare based on binary values (most likely ASCII), or to 'text' to compare more like a human would.
' I.e. ignoring case, umlauts etc. Depends on the regional settings.
'Option Compare Text

Public Module Sorting

    Public IMALIVE As Boolean = True

    Public Function Bubblesort(ByVal l As List(Of String), Optional ByVal Ascending As Boolean = True) As List(Of String)
        Return BubbleAndRippleSort(l, Ascending, True)
    End Function

    Public Function Ripplesort(ByVal l As List(Of String), Optional ByVal Ascending As Boolean = True) As List(Of String)
        Return BubbleAndRippleSort(l, Ascending, False)
    End Function

    Private Function BubbleAndRippleSort(ByVal l As List(Of String), Optional ByVal Ascending As Boolean = True, Optional ByVal Bubble As Boolean = True) As List(Of String)
        ' Given that there is only one minor differences between Bubblesort and Ripplesort (Ripplesort iterating over the whole list all the time, Bubblesort
        ' iterating over shrinking lists), those two are combined here.

        Dim len As Integer = l.Count
        Dim ShiftingHappened As Boolean = True

        ' Looping while a character was changed in the last run, the end of the string is not reached, and the user didn't cancel.
        Do
            ShiftingHappened = False
            For i = 0 To len - 2

                If Ascending Then
                    If Compare(l(i), l(i + 1), True) Then 'l(i) > l(i + 1) 
                        Swap(l, i, i + 1)
                        ShiftingHappened = True
                    End If
                Else
                    If Compare(l(i), l(i + 1), False) Then 'l(i) < l(i + 1)
                        Swap(l, i, i + 1)
                        ShiftingHappened = True
                    End If
                End If

                'frmMain.txtOutput.Text = frmMain.ListToString(l)

            Next

            ' Lo and behold, the only difference between Bubblesort and Ripplesort. The former iterates only over non-sorted parts of the list,
            ' the later over the full list all the time.
            If Bubble Then
                len -= 1
            End If

            Application.DoEvents()

        Loop While ShiftingHappened And len >= 1 And IMALIVE

        Return l

    End Function

    Public Function Selectsort(ByVal l As List(Of String)) As List(Of String)

        Dim IndexOfCurrentLowest As Integer
        Dim Temp As String

        Dim Len As Integer = l.Count

        ' Iterating from the beginning of the list to the second-last item.
        For i = 0 To Len - 2
            If Not IMALIVE Then
                ' If the user canceled the operation, we're bailing out here.
                Return l
            End If

            IndexOfCurrentLowest = i

            ' Selecting the "smallest" character in the list.
            ' This is done by iterating over the remaining list, comparing the currently selected character l(i) with the rest of the items.
            For j = i + 1 To Len - 1
                If Compare(l(j), l(IndexOfCurrentLowest), False) Then 'l(j) < l(IndexOfCurrentLowest)
                    IndexOfCurrentLowest = j
                End If
            Next

            ' Now that we've found the smallest character, we'll append that one to the new list, respectively swap it to the correct place.
            Swap(l, i, IndexOfCurrentLowest)

            'frmMain.txtOutput.Text = frmMain.ListToString(l)

            Application.DoEvents()

        Next

        Return l

    End Function

    Public Function Insertsort(ByVal l As List(Of String), Optional ByVal Ascending As Boolean = True) As List(Of String)

        Dim CharacterToInsert As String
        Dim i As Integer

        For j = 1 To l.Count - 1
            i = j
            ' Grabbing character at index i.
            CharacterToInsert = l(i)
            ' As long as the selected character is smaller than the one to its left, we will exchange the two. (Provided we did not reach the left end of the list, anyway.)
            While i > 0 AndAlso Compare(CharacterToInsert, l(i - 1), False) 'CharacterToInsert < l(i - 1)
                l(i) = l(i - 1)
                l(i - 1) = CharacterToInsert
                i -= 1
            End While

            If Not IMALIVE Then
                ' If the user canceled the operation, we're bailing out here.
                Return l
            End If

            Application.DoEvents()

        Next

        Return l

    End Function

    Public Function Quicksort(ByVal l As List(Of String), Optional ByVal index_begin As Integer = Nothing, Optional ByVal index_end As Integer = Nothing) As List(Of String)
        ' Helper function, with the purpose of making Quicksort() have the same interface as all the other functions.

        If index_begin = Nothing Then
            index_begin = 0
        End If

        If index_end = Nothing Then
            index_end = l.Count - 1
        End If

        ' Creating a copy of the list which we'll work on further, in order not to modify the original list.
        Dim l_copy As List(Of String) = CopyList(l)

        Return _Quicksort(l_copy, index_begin, index_end)

    End Function

    Private Function _Quicksort(ByRef l As List(Of String), ByVal index_begin As Integer, ByVal index_end As Integer) As List(Of String)

        ' Quicksort. I don't fully understand the maths behind it, but it works like a charm.
        Dim i As Integer = index_begin
        Dim j As Integer = index_end

        ' Grabbing a character which is roughly in the middle of the supplied list.
        Dim x As String = l((index_begin + index_end) \ 2)

        If Not IMALIVE Then
            ' If the user cancelled the operation, we're boiling out here.
            Return l
        End If

        Do

            ' Skipping over characters which are smaller than the character in the middle of the supplied list.
            Do While Compare(l(i), x, False) 'l(i) < x
                i += 1
            Loop

            ' Skipping over characters which are greater than the character in the middle of the supplied list.
            Do While Compare(l(j), x) 'l(j) > x
                j -= 1
            Loop

            ' If the list we are left with (that is, the "middle" part between i and j) contains at least one element, we'll swap the outer items.
            If i <= j Then
                Swap(l, i, j)
                i += 1
                j -= 1
            End If

            ' And we'll do so until we're left with an empty list.
        Loop Until i > j

        Application.DoEvents()

        ' Then it's just a matter of calling Quicksort on the partial lists again.
        If index_begin < j Then
            _Quicksort(l, index_begin, j)
        End If

        If index_end > i Then
            _Quicksort(l, i, index_end)
        End If

        Return l

    End Function

    Public Function Magicsort(ByVal l As List(Of String)) As List(Of String)

        ' Using the VB-built-in sorting method, useful to compare self-implemented algorithms to.
        ' Not really a way to influence the treatment of umlauts here. (Not counting the Compare Text setting.)
        l.Sort()
        Return l

    End Function

    Public Function Bogosort(ByVal l As List(Of String)) As List(Of String)

        ' Iterating until it's either sorted, or not alive anymore. (Read: Killed by the user.)
        Do Until IsSorted(l) Or Not IMALIVE
            Shuffle(l)
            Application.DoEvents()
        Loop

        Return l

    End Function

    Private Function IsSorted(ByVal l As List(Of String), Optional ByVal Ascending As Boolean = True) As Boolean

        For i = 0 To l.Count - 2
            If Compare(l(i), l(i + 1)) Then 'l(i) > l(i + 1)
                Return False
            End If
        Next

        Return True

    End Function

    Private Sub Swap(ByRef l As List(Of String), ByVal Index1 As Integer, ByVal Index2 As Integer)
        Dim temp As String = l(Index1)
        l(Index1) = l(Index2)
        l(Index2) = temp
    End Sub

    Private Sub Shuffle(ByRef l As List(Of String))

        Dim RNG As New Random
        Dim NewIndex As Integer

        For i = 0 To l.Count - 1
            NewIndex = RNG.Next(l.Count - 1)
            Swap(l, i, NewIndex)
        Next

    End Sub

    Private Function CopyList(ByVal l As List(Of String)) As List(Of String)

        Dim l_copy As New List(Of String)

        For Each s As String In l
            l_copy.Add(s)
        Next

        Return l_copy

    End Function

    Private Function Compare(ByVal value_1 As String, ByVal value_2 As String, Optional ByVal greater_than As Boolean = True) As Boolean

        ' It's not a pretty solution, but it is a solution. If it wasn't for this function, each algorithm would have to check whether the CompareUmlauts option was set or not.
        ' In order to prevent this redundancy, I've added this function.
        If frmMain.CompareUmlauts Then
            If greater_than Then
                Return value_1 > value_2
            Else
                Return value_1 < value_2
            End If
        Else
            If greater_than Then
                Return StripUmlauts(value_1) > StripUmlauts(value_2)
            Else
                Return StripUmlauts(value_1) < StripUmlauts(value_2)
            End If
        End If

    End Function

    Private Function StripUmlauts(ByVal s As String) As String
        ' Todo: Right. Ugly. But it works.
        Dim s_new As String

        For Each c As Char In s
            s_new &= UmlautToBase(c)
        Next

        Return s_new
    End Function

    Private Function UmlautToBase(ByVal s As String) As String

        Select Case s
            Case "ä", "à", "á", "â", "ã"
                Return "a"
            Case "Ä", "À", "Á", "Â", "Ã"
                Return "A"

            Case "é", "è", "ê", "ë"
                Return "e"
            Case "É", "È", "Ê", "Ë"
                Return "E"

            Case "í", "ì", "î", "ï"
                Return "i"
            Case "Í", "Ì", "Î", "Ï"
                Return "I"

            Case "ö", "ó", "ò", "ô", "õ"
                Return "o"
            Case "Ö", "Ó", "Ò", "Ô", "Õ"
                Return "O"

            Case "ü", "ú", "ù", "û"
                Return "u"
            Case "Ü", "Ú", "Ù", "Û"
                Return "U"

            Case "ñ"
                Return "n"
            Case "Ñ"
                Return "N"

            Case Else ' Rather important, unless you want funny results.
                Return s
        End Select

    End Function

End Module