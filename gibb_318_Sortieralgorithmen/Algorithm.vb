﻿Friend Enum Algorithm As Integer
    Bubblesort = 0
    Ripplesort = 1
    Selectsort = 2
    Insertsort = 3
    Quicksort = 4
    Magicsort = 5
    Bogosort = 6
    UNKNOWN = -1
End Enum